#!/usr/bin/perl

my $waitingTime = 0;
my $longestWaitingTime = 0;
my $waitingTimeSummarized = 0;


#open file with process time, and valid file size
my @fileArray = do {
    open my $fh, "<", "fcfs.txt"
        or die "Nie ma pliku fcfs.txt!";
    <$fh>;
};

if(!@fileArray){
	die "Plik jest pusty";
}


# print "Tryb kolejkowania procesów:\n
# 1.Jak w pliku(Po kolei)\n
# 2.Wymieszaj procesy z pliku\n
# Wybór:";

# my $choose = <STDIN>;

# if($choose > 2 && $choose >= 1){  #todo protect to non numer
# 	die "nie ma takiej opcji w menu!";
# }

# if($choose == 1){
# 	#TODO Shuffle array
# }


#main algoritm
print "[Numer linii]\tCzas fazy\tCzas oczekiwania\tCzas wykonywania\n";

foreach my $i (0 .. @fileArray - 1) {

	$numerLine = $i+1;

	$phaseTime = int($fileArray[$i]);

	$runningTime += $phaseTime;

	#longestWaitingTime
	if($waitingTime > $longestWaitingTime){
		$longestWaitingTime = $waitingTime;
	}

	$waitingTimeSummarized +=$waitingTime;

	print "[$numerLine]\t\t$phaseTime\t\t$waitingTime\t\t\t$runningTime\n";

	$waitingTime += $phaseTime;

}

$mediumWaitingTime = $waitingTimeSummarized / (@fileArray );

print "\nŚredni czas oczekiwania:\t$mediumWaitingTime ms\n";
print "Najdłuższy czas oczekiwania:\t$longestWaitingTime ms\n";