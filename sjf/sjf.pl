#!/bin/perl
@burstTime[10] =();
@ariveTime[10] = () ;
$sumBurstTime=0;
$sumTurnaround=0;
$sumWait=0;

#read files
my @ariveTime= do {
	open my $ariveTime, "<", "sjf_arrive.txt";
	<$ariveTime>;
};

my @burstTime= do {
	open my $burstTime, "<", "sjf_burst.txt";
	<$burstTime>;
};

#length
$numberOfProcess = @burstTime;

#cast to inst, and count burst currentTime
for($i=0;$i< $numberOfProcess;$i++)
{
	$ariveTime[$i] = int($ariveTime[$i]);
	$burstTime[$i] = int($burstTime[$i]);

	$sumBurstTime+=$burstTime[$i];
}

#start, away from array
$burstTime[9999999]=9999;
print "Nr procesu(lini w pliku)|Czas do zakończenia|Czas oczekiwania\n\n";
for($currentTime=0;$currentTime<$sumBurstTime;)
{
	$smallest=9999999; #index away from array

	for($j=0;$j<$numberOfProcess;$j++)
	{
		#********************find smallest************************************
		#if (arrivecurrentTime is lower or eq currentTime from start) AND (burstcurrentTime is bigger than 0) AND  (burstcurrentTime is lower than smallestActyally)
		if(($ariveTime[$j] <= $currentTime) && ($burstTime[$j] > 0) && ($burstTime[$j] < $burstTime[$smallest])){
			$smallest=$j;	#set smallest to actualy
		}
	}
	if($smallest==9999999) #no smallest actualy
	{
		$currentTime++;
		next;
	}
	$tempProcess=$smallest+1; #process id 
	$tempTurnaround=$currentTime+$burstTime[$smallest]-$ariveTime[$smallest]; #actual currentTime + burstcurrentTime smalest - arrive currentTime
	$tempWait= $currentTime-$ariveTime[$smallest]; # actual currentTime - arrive currentTime
	print "$tempProcess\t\t\t\t$tempTurnaround\t\t$tempWait\n";

	#sum for stariveTimes
	$sumTurnaround+=$currentTime+$burstTime[$smallest]-$ariveTime[$smallest];
	$sumWait+=$currentTime-$ariveTime[$smallest];


	$currentTime+=$burstTime[$smallest]; #increment currentTime by processcurrentTime
	$burstTime[$smallest]=0; #process end, can zero burstcurrentTime
}

printf("\nŚrednia czasu oczekiwania = %f\n",$sumWait*1.0/$numberOfProcess);
printf("Średnia czasu wykonywania = %f\n",$sumTurnaround*1.0/$numberOfProcess);

