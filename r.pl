#!/usr/bin/perl

#open file with process time, and valid file size
my @fileArray = do {
    open my $fh, "<", "fcfs.txt"
        or die "Nie ma pliku fcfs.txt!";
    <$fh>;
};

if(!@fileArray){
	die "Plik jest pusty";
}


print "Czas kwanty:";

my $quaterTime = <STDIN>;

my %processQue;

foreach my $i (0 .. @fileArray -1) {

	$processQue{$i}{'processTime'} = int($fileArray[$i]);
	$processQue{$i}{'processTimeCounter'} = 0;
	$processQue{$i}{'completeRunTimer'} = 0;
	$processQue{$i}{'ended'} = 0;

}

$totalEnded = 0;
$totalTime = 0;

while($totalEnded < @fileArray ){

	foreach my $i (0 .. @fileArray -1) {
		if($processQue{$i}{'ended'} == 0){

			$diffrence = int($processQue{$i}{'processTime'} - $processQue{$i}{'processTimeCounter'}) ;

			if($diffrence >= $quaterTime)
			{
				$processQue{$i}{'processTimeCounter'} += $quaterTime;
				$totalTime += $quaterTime;
			}
			elsif(($diffrence < $quaterTime) && ($diffrence != 0) )
			{
			 	$totalTime  += $diffrence;
			 	$processQue{$i}{'processTimeCounter'}  += $diffrence;
			}
			if($processQue{$i}{'processTime'} == $processQue{$i}{'processTimeCounter'})
			{
				$processQue{$i}{'ended'} = 1;
				$processQue{$i}{'completeRunTimer'} = $totalTime;
				$totalEnded++;
			}
			
		}
	}

}

print "[Numer linii]\tCzas procesu\tCałkowity do zakończenia procesu\tCzas oczekiwania\n";

$waitingTimeSum = 0;
$completeTimeSum = 0;
foreach my $i (0 .. @fileArray - 1) {

	$processNumber = $i + 1;
	$waitingTime = $processQue{$i}{'completeRunTimer'} - $processQue{$i}{'processTime'};
	$waitingTimeSum += $waitingTime;
	$completeTimeSum += $processQue{$i}{'completeRunTimer'};

	print "[$processNumber]\t\t$processQue{$i}{'processTime'}\t\t\t$processQue{$i}{'completeRunTimer'}\t\t\t\t$waitingTime\n" ;

}

$waitingTimeAverange = $waitingTimeSum / @fileArray;
$completeTimeAverange = $completeTimeSum / @fileArray;

printf("Średni czas oczekiwania:%.2f ms\n",$waitingTimeAverange);
printf("Średni czas całkowity:%.2f ms\n",$completeTimeAverange);
